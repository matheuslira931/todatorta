var app = angular.module('TodaTorta',[/**'ngSanitize','ngMaterial','ngRoute','ui.ace','ng-file-input','ngTable','ngAnimate','chart.js','dndLists','ngImgCrop'**/]);

app.controller('MainController', function($scope,$http/**,$http,$mdDialog,NgTableParams,ngTableEventsChannel**/) {

    $scope.disabled = [];

    $scope.scrollTo = function(scrollLocation) {
        if (!$scope.disabled[scrollLocation]) {
            $scope.disabled[scrollLocation] = true;
            var sT = $(scrollLocation).offset().top;
            console.log($('#right').css('overflow-y'));        

            if ($('#right').css('overflow-y') == 'auto') {
                $('#right').animate({
                    scrollTop: '+=' + sT
                }, 1000, function(){
                    //alert(scrollLocation);
                    $scope.disabled[scrollLocation] = false;
                });
            }
            else {
                $('html, body').animate({
                    scrollTop: '+=' + sT
                }, 1000, function(){
                    //alert(scrollLocation);
                    $scope.disabled[scrollLocation] = false;
                });
            }
        }
    }       
	
    $scope.validar = function() {
        if ($scope.contato.nome == null) return false;
        if ($scope.contato.telefone == null) return false;
        if ($scope.contato.assunto == null) return false;
        if ($scope.contato.mensagem == null) return false;
        return true;
    }

    $scope.tenteNovamente = function() {
        $scope.erro = false;
    }

    $scope.novaMensagem = function() {
        $scope.enviado = false;
    }


    $scope.enviarContato = function () {
        if ($scope.validar()) {
            $scope.waiting = true;
            $http({
                method: 'POST',
                url: "http://www.etssoft.com.br/gerinusPay/telegram/todatorta",
                data: $scope.contato,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) { 
                console.log(response);
                $scope.contato = {};
                $scope.enviado = true;
            }); 
        }
        else {
            $scope.erro = true;
            console.log('erro');
        }
        
    }

	$scope.init = function() {
		$scope.contato = {
            nome: null,
            telefone: null,
            assunto: null,
            mensagem: null
        };

        $scope.enviado = false;
        $scope.erro = false;

        var token = '2207692169.9949395.f2f63eff806545e7bc0f2954ae4e5cd4';
        $http.get('https://api.instagram.com/v1/users/self/media/recent?access_token=' + token).then(function(response) { 
        	$scope.imgs = response.data.data;
        	console.log($scope.imgs);
        });
	} 
});